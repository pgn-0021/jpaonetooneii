package br.edu.estacio.principal;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.edu.estacio.model.Pessoa;
import br.edu.estacio.model.RegistroGeral;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Compras");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();		
		
		RegistroGeral reg = new RegistroGeral();
		reg.setNumero("123456784");
		em.persist(reg);
		
		Pessoa p = new Pessoa();
		p.setNome("Adriana");
		p.setSobrenome("Santos");
		p.addRegistroGeral(reg);
		reg.addPessoa(p);
		
		em.persist(p);
		em.getTransaction().commit();
				
		
		Pessoa s = em.find(Pessoa.class, 1);
		System.out.println("Nome: "+s.getNome());
		System.out.println("Sobrenome: "+s.getSobrenome());
		System.out.println("Número RG: "+s.getRegistroGeral().getNumero());
		
		RegistroGeral sR = em.find(RegistroGeral.class, 1);
		System.out.println("Reg: "+sR.getNumero());
		System.out.println("Nome: "+sR.getPessoa().getNome());
		System.out.println("Sobrenome: "+sR.getPessoa().getSobrenome());
				
		em.close();
		emf.close();
	}

}
