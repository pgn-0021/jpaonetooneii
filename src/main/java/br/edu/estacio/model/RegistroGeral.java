package br.edu.estacio.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class RegistroGeral {

	@Id
	@GeneratedValue
	private int id;
	
	@Column(length=10)
	private String numero;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="dono_id")
	private Pessoa pessoa;

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}	
	
	public void addPessoa(Pessoa pessoa) {
		this.pessoa=pessoa;
	}
	
	public Pessoa getPessoa() {
		return pessoa;
	}
	
}
