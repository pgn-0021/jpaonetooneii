package br.edu.estacio.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Pessoa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private int id;
	
	private String nome;
	
	private String sobrenome;

	@OneToOne(mappedBy="pessoa")
	private RegistroGeral registrogeral;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
			
	public void addRegistroGeral(RegistroGeral reg) {
		this.registrogeral=reg;
	}
	
	public RegistroGeral getRegistroGeral() {
		return registrogeral;
	}
	
}
